/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Fichier cpp de la classe Robot comportant l’implémentation des méthodes de celle-ci 
 *
 */

#include "Robot.h"



/*
* @brief    Constructeur par defaut de la classe Robot qui initialise les capteurs a zero 
*/
Robot::Robot()
{
   mode_ = MODE_DETECTION;
   manoeuvre_ = DEFAUT;
}

/*
* @brief    Destructeur par defaut
*/
Robot::~Robot(){}

/*
* @brief    Methode qui permet d´aller en mode detection 
* @param    ecran    La classe LCM afin d´utiliser la methode pour afficher sur l´ecran LCD
*/
void Robot::modeDetection(LCM& ecran)
{
   calculerDistances();
   assignerCapteurs();
   trouverManoeuvre();
   affichage_.afficherManoeuvre(ecran, capteurGauche_, capteurMilieu_, capteurDroite_);
}

/*
* @brief    Methode qui permet d´aller en mode manoeuvre 
* @param    ecran    La classe LCM afin d´utiliser la methode pour afficher sur l´ecran LCD
*/
void Robot::modeManoeuvre(LCM& ecran)
{
   affichage_.afficherModeManoeuvre(ecran, manoeuvre_);
   moteur_.executerManoeuvre(manoeuvre_);
   mode_ = MODE_DETECTION;
}

/*
* @brief    Methode qui permet de trouver la manoeuvre en mode detection 
*/
void Robot::trouverManoeuvre()
{
    if (capteurGauche_.getEtatCapteur() == OK && capteurMilieu_.getEtatCapteur() == OK && capteurDroite_.getEtatCapteur() == ATTN)
        manoeuvre_ = M1;
    else if (capteurGauche_.getEtatCapteur() == ATTN && capteurMilieu_.getEtatCapteur() == OK && capteurDroite_.getEtatCapteur() == OK)
        manoeuvre_ = M2;
    else if (capteurGauche_.getEtatCapteur() == OK && capteurMilieu_.getEtatCapteur() == DNGR && capteurDroite_.getEtatCapteur() == DNGR)
        manoeuvre_ = M3;
    else if (capteurGauche_.getEtatCapteur() == DNGR && capteurMilieu_.getEtatCapteur() == DNGR && capteurDroite_.getEtatCapteur() == OK)
        manoeuvre_ = M4;
    else if (capteurGauche_.getEtatCapteur() == DNGR && capteurMilieu_.getEtatCapteur() == DNGR && capteurDroite_.getEtatCapteur() == DNGR)
        manoeuvre_ = M5;
    else if (capteurGauche_.getEtatCapteur() == ATTN && capteurMilieu_.getEtatCapteur() == OK && capteurDroite_.getEtatCapteur() == ATTN)
        manoeuvre_ = M6;
    else
        manoeuvre_ = DEFAUT;
}

/*
* @brief    Methode qui permet d´assigner l´etat du capteur (DNGR, OK, ATTN)
*           selon les bonnes distances entre les obstacles et les capteurs pour chaque capteur
*/
void Robot::assignerCapteurs()
{
   capteurDroite_.assignerCapteur();
   capteurMilieu_.assignerCapteur();
   capteurGauche_.assignerCapteur();
}

/*
* @brief    Methode qui qui permet d´assigner les distances, provenant de la detection, pour chaque capteur
*/
void Robot::calculerDistances()
{
   capteurDroite_.calculerDistance();
   _delay_ms(60);
   capteurMilieu_.calculerDistance();
   _delay_ms(60);
   capteurGauche_.calculerDistance();
   _delay_ms(60);
}

Moteur& Robot::getMoteur()
{
   return moteur_;
}

/*
* @brief    Methode qui retourne la distance de l´attribut capteurGauche_
*/
Capteur& Robot::getCapteurGauche()
{
   return capteurGauche_;
}

/*
* @brief    Methode qui retourne la distance de l´attribut capteurMilieu_
*/
Capteur& Robot::getCapteurMilieu()
{
   return capteurMilieu_;
}

/*
* @brief    Methode qui retourne la distance de l´attribut capteurDroite_
*/
Capteur& Robot::getCapteurDroite()
{
   return capteurDroite_;
}

/*
* @brief    Methode qui retourne le mode de l´attribut mode_
*/
Mode Robot::getModeRobot()
{
   return mode_;
}

/*
* @brief    Methode qui permet d´affecter un mode a l´attribut mode_
* @param    mode     L´enum Mode 
*/
void Robot::setMode(const Mode& mode)
{
   mode_ = mode;
}