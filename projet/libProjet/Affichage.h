/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Fichier .h de la classe Affichage qui permet de regrouper les methodes utilisees pour l´affichage sur l´ecran LCD
 * 
 * PIN 1 a 7 du port B et DDRB est en mode sortie
 */ 

#include "lcm_so1602dtr_m_fw.h"
#include "lcm_so1602dtr_m.h"
#include "customprocs.h"
#ifndef F_CPU
#define F_CPU 8000000UL //8 MHz
#endif
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "Capteurs.h"


class Affichage {
public:
    void afficherManoeuvre(LCM&, Capteur&,Capteur&,Capteur&);
    void afficherDistances(LCM&, Capteur&,Capteur&,Capteur&);
    void afficherModeManoeuvre(LCM& ,Manoeuvre);
    void afficherModeM1(LCM&);
    void afficherModeM2(LCM&);
    void afficherModeM3(LCM&);
    void afficherModeM4(LCM&);
    void afficherModeM5(LCM&);
    void afficherModeM6(LCM&);
    void afficherModeNonEvaluee(LCM&);
};