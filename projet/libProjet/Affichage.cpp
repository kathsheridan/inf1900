/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Fichier cpp de la classe Affichage comportant l’implémentation des méthodes de celle-ci pour afficher sur le LCD
 * 
 * PIN 1 a 7 du port B et DDRB est en mode sortie
 */

#include "Affichage.h"
static const uint8_t DEBUT_DEUXIEME_LIGNE = 16;


/*
* @brief    Methode qui permet d´afficher l´etat de chaque capteur si les combinaisons ne font pas parties des 6 manoeuvres
* @param    La classe LCM afin d´utiliser la methode pour afficher sur l´ecran LCD
* @param    capteurGauche    La classe Capteur afin de trouver l´etat du capteur gauche
* @param    capteurMilieu    La classe Capteur afin de trouver l´etat du capteur milieu
* @param    capteurDroite    La classe Capteur afin de trouver l´etat du capteur droit
*/
void Affichage::afficherManoeuvre(LCM& ecran, Capteur& capteurGauche,Capteur& capteurMilieu,Capteur& capteurDroite)
{
   ecran.clear();
   afficherDistances(ecran, capteurGauche, capteurMilieu, capteurDroite);
   if(capteurGauche.getEtatCapteur() == OK)
      ecran.write("OK",DEBUT_DEUXIEME_LIGNE + 1,false);
   else if((capteurGauche.getEtatCapteur() == ATTN))
      ecran.write("ATTN",DEBUT_DEUXIEME_LIGNE,false);
   else 
      ecran.write("DNGR",DEBUT_DEUXIEME_LIGNE,false);
   
   if(capteurMilieu.getEtatCapteur() == OK)
      ecran.write("OK",DEBUT_DEUXIEME_LIGNE+7,false);
   else if(capteurMilieu.getEtatCapteur() == ATTN)
      ecran.write("ATTN",DEBUT_DEUXIEME_LIGNE+6,false);
   else
      ecran.write("DNGR",DEBUT_DEUXIEME_LIGNE+6,false);
   
   if(capteurDroite.getEtatCapteur() == OK)
      ecran.write("OK",DEBUT_DEUXIEME_LIGNE+13,false);
   else if (capteurDroite.getEtatCapteur() == ATTN)
      ecran.write("ATTN",DEBUT_DEUXIEME_LIGNE+12,false);
   else
      ecran.write("DNGR",DEBUT_DEUXIEME_LIGNE+12,false);
}

/*
* @brief    Methode qui permet d´afficher les manoeuvres correspondantes en mode manoeuvre
* @param    ecran       La classe LCM afin d´utiliser la methode pour afficher sur l´ecran LCD
* @param    manoeuvre   L´enum Manoeuvre afin de trouver quelle mode manoeuvre afficher apres la detection
*/
void Affichage::afficherModeManoeuvre(LCM& ecran, Manoeuvre manoeuvre)
{
    switch(manoeuvre)
    {
    case M1:
        ecran.clear();
        afficherModeM1(ecran);
        break;
    case M2:
        ecran.clear();
        afficherModeM2(ecran);
        break;
    case M3:
        ecran.clear();
        afficherModeM3(ecran);
        break;
    case M4:
        ecran.clear();
        afficherModeM4(ecran);
        break;
    case M5:
        ecran.clear();
        afficherModeM5(ecran);
        break;
    case M6:
        ecran.clear();
        afficherModeM6(ecran);
        break;
    case DEFAUT:
        ecran.clear();
        afficherModeNonEvaluee(ecran);
        break;
    }
}

/*
* @brief    Methode qui permet d´afficher le modele pour les distances sur le LCD
* @param    La classe LCM afin d´utiliser la methode pour afficher sur l´ecran LCD
* @param    capteurGauche    La classe Capteur afin de trouver l´etat du capteur gauche 
* @param    capteurMilieu    La classe Capteur afin de trouver l´etat du capteur milieu
* @param    capteurDroite    La classe Capteur afin de trouver l´etat du capteur droit
*/
void Affichage::afficherDistances(LCM& ecran, Capteur& capteurGauche,Capteur& capteurMilieu,Capteur& capteurDroite)
{
   ecran << capteurGauche.getDistanceString().premierChiffre << '.' << capteurGauche.getDistanceString().deuxiemeChiffre << "m  " << capteurMilieu.getDistanceString().premierChiffre 
   << '.' << capteurMilieu.getDistanceString().deuxiemeChiffre << "m  " << capteurDroite.getDistanceString().premierChiffre
   << '.' << capteurDroite.getDistanceString().deuxiemeChiffre << "m  ";
}

/*
* @brief    Afficher Manoeuvre 1 sur le LCD en mode manoeuvre
* @param    la classe LCM pour ecrire sur le LCD
*/
void Affichage::afficherModeM1(LCM& ecran_)
{
    ecran_.write("Manoeuvre 1");
}

/*
* @brief    Afficher Manoeuvre 2 sur le LCD en mode manoeuvre
* @param    la classe LCM pour ecrire sur le LCD
*/
void Affichage::afficherModeM2(LCM& ecran_)
{
     ecran_.write("Manoeuvre 2");
}

/*
* @brief    Afficher Manoeuvre 3 sur le LCD en mode manoeuvre
* @param    la classe LCM pour ecrire sur le LCD
*/
void Affichage::afficherModeM3(LCM& ecran_)
{
     ecran_.write("Manoeuvre 3");
}

/*
* @brief    Afficher Manoeuvre 4 sur le LCD en mode manoeuvre
* @param    la classe LCM pour ecrire sur le LCD
*/
void Affichage::afficherModeM4(LCM& ecran_)
{
     ecran_.write("Manoeuvre 4");
}

/*
* @brief    Afficher Manoeuvre 5 sur le LCD en mode manoeuvre
* @param    la classe LCM pour ecrire sur le LCD
*/
void Affichage::afficherModeM5(LCM& ecran_)
{
     ecran_.write("Manoeuvre 5");
}

/*
* @brief    Afficher Manoeuvre 6 sur le LCD en mode manoeuvre
* @param    la classe LCM pour ecrire sur le LCD
*/
void Affichage::afficherModeM6(LCM& ecran_)
{
     ecran_.write("Manoeuvre 6");
}

/*
* @brief    Afficher Combinaison non evaluee sur le LCD en mode manoeuvre
* @param    la classe LCM pour ecrire sur le LCD
*/
void Affichage::afficherModeNonEvaluee(LCM& ecran_)
{
     ecran_.write("Combinaison\nnon evaluee");
     _delay_ms(2000);
}
