/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Fichier .h de la classe Timer0 qui permet de regrouper les methodes utilisees pour modifier les registres pour le timer/counter0
 *
 */

#ifndef F_CPU
# define F_CPU 8000000UL //8 MHz
#endif
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>


class Timer0 
{
    public:
        Timer0();
        void setTcnt0(uint16_t);
        void setTimsk(char,char,char);
        void setOicr0a(uint16_t);
        void setOicr0b(uint16_t);
        void setTccr0a(uint8_t,uint8_t,uint8_t,uint8_t,uint8_t,uint8_t,uint8_t);
        void setTccr0b(uint8_t,uint8_t,uint8_t);
        void setNormalModeOverflow();
        void setModeCTCsetOnCompareMatchA();
        void setModeCTCclearOnCompareMatchA();
        void setModeCTCtoggleOnCompareMatchA();
        void setModePhaseCorrectClearUpcountingA();
        void setModePhaseCorrectSetUpcountingA();
        void setModeCTCsetOnCompareMatchB();
        void setModeCTCclearOnCompareMatchB();
        void setModeCTCtoggleOnCompareMatchB();
        void setModePhaseCorrectClearUpcountingB();
        void setModePhaseCorrectSetUpcountingB();
        void setPrescaler1();
        void setPrescaler8();
        void setPrescaler64();
        void setPrescaler256();
        void setPrescaler1024();

};