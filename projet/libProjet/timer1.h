/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Fichier .h de la classe Moteur qui permet de regrouper les methodes utilisees pour modifier les registres pour le timer/counter 1
 *
 */

#ifndef F_CPU
# define F_CPU 8000000UL //8 MHz
#endif
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>

class Timer1 
{
    public:
        Timer1();
        void setTcnt1(uint16_t);
        void setTimsk(char,char,char);
        void setOicr1a(uint16_t);
        void setOicr1b(uint16_t);
        void setTccr1a(uint8_t,uint8_t,uint8_t,uint8_t,uint8_t,uint8_t,uint8_t,uint8_t);
        void setTccr1b(uint8_t,uint8_t,uint8_t);
        void setTccr1c(uint8_t);
        void setModeCTCsetOnCompareMatchA();
        void setModeCTCclearOnCompareMatchA();
        void setModeCTCtoggleOnCompareMatchA();
        void setModePhaseCorrectClearUpcountingA();
        void setModePhaseCorrectSetUpcountingA();
        void setModeCTCsetOnCompareMatchB();
        void setModeCTCclearOnCompareMatchB();
        void setModeCTCtoggleOnCompareMatchB();
        void setModePhaseCorrectClearUpcountingB();
        void setModePhaseCorrectClearUpcountingAB();
        void setModePhaseCorrectSetUpcountingB();
        void setPrescaler1();
        void setPrescaler8();
        void setPrescaler64();
        void setPrescaler256();
        void setPrescaler1024();

};

