/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait et Katherine Sheridan
 * 2020
 *
 * Fichier cpp de la classe Capteur comportant l’implémentation des méthodes de celle-ci
 * PIN 0 a 3 du port A, A0 est en mode sortie , A1 a A3 en mode entrée et le reste en sortie
 */
#define F_CPU 8000000UL //8 MHz
#include <util/delay.h>
#include "Capteurs.h"

volatile uint8_t nbCycles = 0;

/*
* @brief    Constructeur par défaut qui initiliase les attributs distance_  et valeurMasque_ à 0 ainsi que etatCapteur_ a DNGR
*/
Capteur::Capteur(): distance_(0), etatCapteur_(DNGR), valeurMasque_(0){};


ISR(TIMER0_OVF_vect)
{
   nbCycles++;
}

/*
* @brief    Destructeur par defaut
*/
Capteur::~Capteur()
{
}


/*
* @brief    Définit l’attribut valeurMasque_ de la classe capteur
* @param    valeurMasque   valeurMasque valeur en hexadécimal représentant le masque nécéssaire pour traiter la pin connectée au Echo du capteur
*/
void Capteur::setValeurMasque(const uint8_t valeurMasque)
{
   valeurMasque_ = valeurMasque;
}

/*
* @brief    Lance le capteur et attend que la pin Echo tourne à 1.
*/
void Capteur::initialiserCapteur()
{
   PORTB = 0x01;
   _delay_us(10);
   PORTB = 0x00;
    
   while(!(PINA & valeurMasque_))
   {}

}

/*
* @brief    Fonction qui met en place le timer0 et le lance pour qu'il commence à compter 
*           le temps que ca prend pour avoir un rebond entre le capteur et l´objet
*/
void Capteur::lancerTimer0()
{
   cli();
   sei();
   TIMSK0 = 1<<TOIE0;
   TCCR0A = 0;
   TCCR0B = (1 << CS01);
}

/*
* @brief    Calculer le temps de retour des ondes emis du capteur vers l´objet, et de l´objet vers le capteur 
*           pour avoir la distance emis par la source de tension. Cette methode permet de faire une conversion en metre 
*           par rapport au voltage choisi sur la source de tension
*/
void Capteur::calculerDistance()
{
   initialiserCapteur();
   float tempsAllerRetour;   
   nbCycles = 0;
   lancerTimer0();  
   while(PINA & valeurMasque_)
   {}
   TCCR0B = 0;
   tempsAllerRetour = (TCNT0 + nbCycles*VALEUR_MAX_8BITS);
   distance_ = tempsAllerRetour/FACTEUR_US_VERS_METRE;

}

/*
* @brief    Definir l´etat du capteur (DNGR, OK, ATTN) selon les bonnes distances entre les obstacles et les capteurs
*/
void Capteur::assignerCapteur()
{
   if(distance_ < VALEUR_MAX_DNGR)
      etatCapteur_ = DNGR;
   else if(distance_ >= VALEUR_MAX_DNGR && distance_ < VALEUR_MIN_OK)
      etatCapteur_ = ATTN;
   else
      etatCapteur_ = OK;
}

/*
* @brief    Methode qui retourne l´etat du capteur (l´attribut) (DNGR, ATTN ou OK)
*/
EtatCapteur Capteur::getEtatCapteur()
{
   return etatCapteur_;
}

/*
* @brief    Methode qui affecte une distance a l´attribut distance_
* @param    distance    Une distance quelconque
*/
void Capteur::setDistance(float distance)
{
   distance_ = distance;
}


/*
* @brief    Methode qui retourne la distance (l´attribut) entre
*/
double Capteur::getDistance()
{
   return distance_;
}

/*
* @brief    Methode qui permet d´arrondir le nombre (distance_) a un chiffre apres la virgule
* @return   Retourne la distance avec 1 chiffre avant la virgule et 1 apres la virgule
*/
Distance Capteur::getDistanceString()
{
   Distance distanceString;
   distanceString.premierChiffre = distance_;
   distanceString.deuxiemeChiffre = (distance_ - distanceString.premierChiffre)*FACTEUR_DEPLACEMENT_VIRGULE_VERS_GAUCHE;
   return distanceString;

}


