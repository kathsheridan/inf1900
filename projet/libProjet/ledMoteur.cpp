/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Fichier cpp de la classe LED comportant l’implémentation des méthodes de celle-ci
 * 
 * PIN 0 et 1 portD pour la gauche (PD0 et PD1) et PIN 2 et pour la droite (PD2 et PD7) du port D.
 * DDRD en mode sortie
 */

#include "ledMoteur.h"


/*
* @brief    Allumer la led de gauche en vert et de droite en vert
*/
void LED::ledVerteVerte()
{
    PORTD |= (1 << PD0);		//LED verte gauche car avance
    PORTD &=~(1<<PD1);          //inverse polarite pour eteindre rouge gauche
    PORTD |= (1 << PD2);		//LED verte droite car avance
    PORTD &=~(1<<PD7);          //inverse polarite pour eteindre rouge droite
}

/*
* @brief    Allumer la led de gauche en vert et de droite en rouge
*/
void LED::ledVerteRouge()
{
    PORTD |= (1 << PD0);		//LED verte gauche car avance
    PORTD &=~(1<<PD1);          //inverse polarite pour eteindre rouge gauche
    PORTD &=~(1 << PD2);		//inverse polarite pour eteindre verte droite
    PORTD |= (1<<PD7);			//LED rouge droite car recule
}

/*
* @brief    Allumer la led de gauche en rouge et de droite en vert
*/
void LED::ledRougeVerte()
{
    PORTD &= ~(1 << PD0);		//inverse polarite pour eteindre verte gauche
    PORTD |= (1<<PD1);			//LED rouge gauche car recule
    PORTD |= (1 << PD2);		//LED verte droite car avance
    PORTD &=~(1<<PD7);          //inverse polarite pour eteindre rouge droite
}